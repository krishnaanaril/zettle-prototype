export class User {
    id?: string;
    email?: string;
    password?: string;
    name?: string;
    token?: string;
    vpa?: string;
    tel?: string;
}
