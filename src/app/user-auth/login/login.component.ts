import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, Validators } from '@angular/forms';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducers';
import { LogIn } from 'src/app/store/actions/auth.actions';
import { User } from '../model/user';
import { Router } from '@angular/router';

const loginUser = gql`
mutation loginUser($email: String!, $password: String!) {
  login (
    email:$email
    password:$password
  ){
    user {
      name
      id
    }
    token
  }
}
`;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    hideRequired: ['']
  });

  constructor(private titleService: Title,
    private fb: FormBuilder,
    private apollo: Apollo,
    private store: Store<{ authState: State }>,
    private router: Router) { }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Login');
  }

  onSubmit() {
    console.log(this.loginForm.get('email').value);
    this.apollo.mutate({
      mutation: loginUser,
      variables: {
        email: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value
      }
    }).subscribe(({ data }) => {
      console.log('got data', data);
      localStorage.setItem('userId', data.login.user.id);
      this.router.navigate(['/customer']);
    }, (error) => {
      console.log('there was an error sending the query', error);
    });
  }

}
