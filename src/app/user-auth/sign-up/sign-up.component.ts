import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, Validators } from '@angular/forms';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

const loginUser = gql`
mutation signupUser($email: String!, $password:String!, $name:String!) {
    signup(
      email:$email
      password:$password
      name:$name
    ){
      user{
        name
      }
      token
    }
  }
`;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signupForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    name: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  });

  // products: Observable<Products[]>;

  constructor(private titleService: Title,
    private fb: FormBuilder,
    private apollo: Apollo) { }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Register');
  }

  onSubmit() {
    this.apollo.mutate({
      mutation: loginUser,
      variables: {
        email: this.signupForm.get('email').value,
        password: this.signupForm.get('password').value,
        name: this.signupForm.get('name').value
      }
    }).subscribe(({ data }) => {
      console.log('got data', data);
    }, (error) => {
      console.log('there was an error sending the query', error);
    });
  }

}
