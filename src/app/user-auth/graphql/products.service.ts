import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';

export interface Products {
  id: String;
  name: String;
  description: String;
  price: String;
}

export interface Response {
  allProducts: Products[];
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends Query<Response> {

  document = gql`
    query allProducts {
      productCatalogs {
        name
        id
        description
        price
      }
    }
  `;
}
