import gql from 'graphql-tag';

export class Mutations {
    public static readonly signupUser = gql`
        mutation signupUser($email: String!,
            $password: String!,
            $name: String!) {
                signup(email: $email,
                    password: $password,
                    $name: name) {
                        token
                }
        }
    `;
}
