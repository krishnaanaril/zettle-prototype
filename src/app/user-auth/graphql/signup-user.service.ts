import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class SignupUserService extends Mutation {

  document = gql`
  mutation signupUser($email: String!,
      $password: String!,
      $name: String!) {
          signup(email: $email,
              password: $password,
              $name: name) {
                  token
          }
  }
`;
}
