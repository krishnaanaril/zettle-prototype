import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'user-auth',
    loadChildren: './user-auth/user-auth.module#UserAuthModule'
  },
  {
    path: 'business',
    loadChildren: './business/business.module#BusinessModule'
  },
  {
    path: 'business-admin',
    loadChildren: './business-admin/business-admin.module#BusinessAdminModule'
  },
  {
    path: 'customer',
    loadChildren: './customer/customer.module#CustomerModule'
  },
  { path: '', redirectTo: '/landing', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
