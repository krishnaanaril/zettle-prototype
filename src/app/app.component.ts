import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'zettle-proto';

  constructor(iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'add',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-add-24px.svg'));
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-edit-24px.svg'));
    iconRegistry.addSvgIcon(
      'delete',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-delete-24px.svg'));
    iconRegistry.addSvgIcon(
      'details',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-details-24px.svg'));
    iconRegistry.addSvgIcon(
      'bill-add',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-note_add-24px.svg'));
    iconRegistry.addSvgIcon(
      'more-vert',
      sanitizer.bypassSecurityTrustResourceUrl('assets/mat-icons/baseline-more_vert-24px.svg'));
  }
}
