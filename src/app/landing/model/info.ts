export class Info {
    public title: String;
    public message: String;
    public imageUrl: String;
    public align: String;
}
