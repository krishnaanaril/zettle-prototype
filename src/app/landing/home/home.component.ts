import { Component, OnInit } from '@angular/core';

import { DataService } from '../services/data.service';
import { Info } from '../model/info';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  landingInfo: Info[];

  constructor(private service: DataService) { }

  ngOnInit() {
    this.service.getLandingData().subscribe((res) => {
      this.landingInfo = res;
    },
      (error) => {
        console.log(error);
      });
  }

}
