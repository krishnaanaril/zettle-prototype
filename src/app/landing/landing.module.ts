import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { LandingRoutingModule } from './landing-routing.module';
import { MaterialModule } from '../material.module';
import { HomeComponent } from './home/home.component';
import { JumbotronComponent } from './component/jumbotron/jumbotron.component';
import { UserSignupComponent } from './component/user-signup/user-signup.component';
import { SiteNavigationComponent } from './component/site-navigation/site-navigation.component';


@NgModule({
  declarations: [HomeComponent, JumbotronComponent, UserSignupComponent, SiteNavigationComponent],
  imports: [
    HttpClientModule,
    MaterialModule,
    CommonModule,
    LandingRoutingModule
  ]
})
export class LandingModule { }
