import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss']
})
export class JumbotronComponent implements OnInit {

  @Input() title: String;
  @Input() message: String;
  @Input() imageUrl: String;
  @Input() align: String;

  backgroundStyle: {};
  currentClasses: {};

  constructor() { }

  ngOnInit() {
    this.backgroundStyle = { 'background-image': `url("${this.imageUrl}")` };
    this.currentClasses = {
      'align-right': this.align === 'right',
      'align-left': this.align === 'left'
    };
  }
}
