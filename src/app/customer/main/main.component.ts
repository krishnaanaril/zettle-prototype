import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducers';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  authState$: Observable<State>;

  constructor(
    private router: Router,
    private store: Store<{ authState: State }>) {
    this.authState$ = store.pipe(select('authState'));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  signOut() {
    localStorage.removeItem('userId');
    this.router.navigate(['/landing']);
  }

}
