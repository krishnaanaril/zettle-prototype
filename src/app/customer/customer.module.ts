import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { MaterialModule } from '../material.module';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ActivityComponent } from './component/activity/activity.component';
import { SharedModule } from '../shared/shared.module';
import { TotalExpenseComponent } from './component/total-expense/total-expense.component';


@NgModule({
  declarations: [MainComponent, DashboardComponent, ActivityComponent, TotalExpenseComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    CustomerRoutingModule
  ]
})
export class CustomerModule { }
