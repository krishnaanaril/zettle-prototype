import { Component, OnInit } from '@angular/core';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { DataService } from '../../service/data.service';
import { Activity } from '../../model/activity';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  activities: Activity[];

  constructor(private titleService: Title,
    private service: DataService) {
  }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Your user transactions');
    this.service.getUserActivity().subscribe((res) => {
      this.activities = res;
    },
      (error) => {
        console.log(error);
      });
  }

}
