import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingModule } from './landing/landing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
// import { UserAuthModule } from './user-auth/user-auth.module';
// import { BusinessModule } from './business/business.module';
// import { BusinessAdminModule } from './business-admin/business-admin.module';
// import { CustomerModule } from './customer/customer.module';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { authReducer } from './store/reducers/auth.reducers';
// import { EffectsModule } from '@ngrx/effects';
// import { AppEffects } from './app.effects';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LandingModule,
    // UserAuthModule,
    // BusinessModule,
    // BusinessAdminModule,
    // CustomerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    GraphQLModule,
    HttpClientModule,
    StoreModule.forRoot({ authState: authReducer})
    // EffectsModule.forRoot([AppEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
