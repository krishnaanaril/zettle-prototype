
import { Action } from '@ngrx/store';
import { State } from '../reducers/auth.reducers';

export enum ActionTypes {
    LogIn = '[UserAuth Component] LogIn',
    LogOut = '[UserAuth Component] LogOut'
}

export class LogIn implements Action {
    readonly type = ActionTypes.LogIn;
    constructor(public payload: State) { }
}

export class LogOut implements Action {
    readonly type = ActionTypes.LogOut;
    constructor(public payload: State) { }
}

export type AuthActions = LogIn | LogOut;
