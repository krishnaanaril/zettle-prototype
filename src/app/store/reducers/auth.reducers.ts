import { User } from 'src/app/user-auth/model/user';
import { ActionTypes, AuthActions } from '../actions/auth.actions';

export interface State {
    // is a user authenticated?
    isAuthenticated: boolean;
    // if authenticated, there should be a user object
    user: User | null;
    // error message
    errorMessage: string | null;
}

export const initialState: State = {
    isAuthenticated: false,
    user: null,
    errorMessage: null
};


export function authReducer(state = initialState, action: AuthActions) {
    switch (action.type) {
        case ActionTypes.LogIn:
            if (action.payload) {
                return state = action.payload;
            }
            break;
        case ActionTypes.LogOut:
            return state = initialState;
        default:
            return state;
    }
}
