export class ProductList {
    public productName: String;
    public manufacturerName: String;
    public quantity: String;
    public price: String;
}
