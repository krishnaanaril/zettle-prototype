import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  constructor(private bottomSheetRef: MatBottomSheetRef<AddProductComponent>) { }

  ngOnInit() {
  }

  cancelClick() {
    this.bottomSheetRef.dismiss();
  }

}
