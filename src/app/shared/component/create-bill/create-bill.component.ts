import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ProductList } from '../../model/product-list';
import { DataService } from '../../service/data.service';
import { MatBottomSheet } from '@angular/material';
import { AddProductComponent } from '../add-product/add-product.component';

export interface User {
  name: string;
}


@Component({
  selector: 'app-create-bill',
  templateUrl: './create-bill.component.html',
  styleUrls: ['./create-bill.component.scss']
})
export class CreateBillComponent implements OnInit {

  myControl = new FormControl();
  options: User[] = [
    { name: 'Mary' },
    { name: 'Shelley' },
    { name: 'Igor' }
  ];
  filteredOptions: Observable<User[]>;

  productList: ProductList[];

  constructor(private service: DataService,
    private bottomSheet: MatBottomSheet) { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | User>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.options.slice())
      );

    this.service.getProductList().subscribe((res) => {
      this.productList = res;
    },
      (error) => {
        console.log(error);
      });
  }

  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  addProduct(): void {
    this.bottomSheet.open(AddProductComponent);
  }

}
