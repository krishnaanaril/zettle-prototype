import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedRoutingModule } from './shared-routing.module';
import { CreateBillComponent } from './component/create-bill/create-bill.component';
import { MaterialModule } from '../material.module';
import { AddProductComponent } from './component/add-product/add-product.component';


@NgModule({
  declarations: [CreateBillComponent, AddProductComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedRoutingModule
  ],
  entryComponents: [AddProductComponent]
})
export class SharedModule { }
