import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { UserBusinessComponent } from './component/user-business/user-business.component';
import { ZettleBusinessComponent } from './component/zettle-business/zettle-business.component';
import { BusinessSettingsComponent } from './component/business-settings/business-settings.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: 'existing', component: UserBusinessComponent },
      { path: 'new-business', component: ZettleBusinessComponent },
      { path: 'settings', component: BusinessSettingsComponent },
      { path: '', redirectTo: 'existing', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessAdminRoutingModule { }
