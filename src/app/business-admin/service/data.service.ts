import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getBusinessCategories(): Observable<any> {
    return this.http.get('../../../assets/json/store-data.json');
  }

  public getUserBusinesses(): Observable<any> {
    return this.http.get('../../../assets/json/user-store-data.json');
  }
}
