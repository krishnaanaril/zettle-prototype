import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserStoreInfo } from '../../model/user-store-info';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-user-business',
  templateUrl: './user-business.component.html',
  styleUrls: ['./user-business.component.scss']
})
export class UserBusinessComponent implements OnInit {

  userStoreInfo: UserStoreInfo[];

  constructor(private titleService: Title,
    private service: DataService) { }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Your existing businesses');
    this.service.getUserBusinesses().subscribe((res) => {
      this.userStoreInfo = res;
    },
      (error) => {
        console.log(error);
      });
  }

}
