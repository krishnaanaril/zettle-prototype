import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-business-settings',
  templateUrl: './business-settings.component.html',
  styleUrls: ['./business-settings.component.scss']
})
export class BusinessSettingsComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  cancelClick() {
    this.location.back();
  }

}
