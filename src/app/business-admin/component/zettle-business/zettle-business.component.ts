import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { StoreInfo } from '../../model/store-info';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-zettle-business',
  templateUrl: './zettle-business.component.html',
  styleUrls: ['./zettle-business.component.scss']
})
export class ZettleBusinessComponent implements OnInit {

  storeInfo: StoreInfo[];

  constructor(private titleService: Title,
    private service: DataService) { }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Start a new business with Zettle');
    this.service.getBusinessCategories().subscribe((res) => {
      this.storeInfo = res;
    },
      (error) => {
        console.log(error);
      });
  }

}
