import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZettleBusinessComponent } from './zettle-business.component';

describe('ZettleBusinessComponent', () => {
  let component: ZettleBusinessComponent;
  let fixture: ComponentFixture<ZettleBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZettleBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZettleBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
