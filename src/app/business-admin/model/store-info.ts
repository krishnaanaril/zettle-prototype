export class StoreInfo {
    public title: String;
    public imageUrl: String;
    public description: String;
}
