export class UserStoreInfo {
    public name: String;
    public address: String;
    public city: String;
    public state: String;
    public gstNo: String;
    public open: String;
    public close: String;
}

