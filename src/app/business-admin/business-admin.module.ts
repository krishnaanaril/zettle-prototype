import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { BusinessAdminRoutingModule } from './business-admin-routing.module';
import { MainComponent } from './main/main.component';
import { UserBusinessComponent } from './component/user-business/user-business.component';
import { ZettleBusinessComponent } from './component/zettle-business/zettle-business.component';
import { BusinessSettingsComponent } from './component/business-settings/business-settings.component';
import { MaterialModule } from '../material.module';


@NgModule({
  declarations: [MainComponent, UserBusinessComponent, ZettleBusinessComponent, BusinessSettingsComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    MaterialModule,
    BusinessAdminRoutingModule
  ]
})
export class BusinessAdminModule { }
