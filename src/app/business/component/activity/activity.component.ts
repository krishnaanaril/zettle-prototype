import { Component, OnInit } from '@angular/core';
import { Activity } from '../../model/activity';
import { Title } from '@angular/platform-browser';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  activities: Activity[];

  constructor(private titleService: Title,
    private service: DataService) {
  }

  ngOnInit() {
    this.titleService.setTitle('Zettle - Your user transactions');
    this.service.getBusinessActivity().subscribe((res) => {
      this.activities = res;
    },
      (error) => {
        console.log(error);
      });
  }

}
