export class Activity {
    public biller: String;
    public payee: String;
    public itemCount: String;
    public total: String;
    public currencyCode: String;
    public date: String;
}
