import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessRoutingModule } from './business-routing.module';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ActivityComponent } from './component/activity/activity.component';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { TotalSalesComponent } from './component/total-sales/total-sales.component';
import { TotalOrdersComponent } from './component/total-orders/total-orders.component';
import { AverageOrderValueComponent } from './component/average-order-value/average-order-value.component';

@NgModule({
  declarations: [MainComponent, DashboardComponent, ActivityComponent, TotalSalesComponent, TotalOrdersComponent, AverageOrderValueComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    BusinessRoutingModule
  ]
})
export class BusinessModule { }
